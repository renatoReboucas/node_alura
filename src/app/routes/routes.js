const LivroDao = require('../controller/livro.dao');
const db = require('../../config/database')
module.exports = (app) => {

    app.get('/', function (req, resp) {
        resp.send(
            `
       <html>
            <body> <h1> Entrou resp EXPRESS </h1> </body>
        </html>
    `
        );
    });

    app.get('/livros', function (req, resp) {
        const livroDao = new LivroDao(db);
        livroDao.lista()
            .then(livros => resp.marko(
                require('../views/livros/lista/lista.marko'), {
                    livros: livros
                }
            ))
            .catch(erro => console.log(erro));
    });
    
    app.get('/livros/form', function(req, resp){
        resp.marko(require('../views/livros/form/form.marko') )
    });

    app.post('/livros', function(req, resp){
        //imprime dados do post
        console.log(req.boby);
    });

}
